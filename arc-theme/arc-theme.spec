%global common_configure --with-gnome-shell=3.34

%global common_desc Arc is a flat theme with transparent elements for GTK 3, GTK 2 and GNOME Shell, Unity, Pantheon, Xfce, MATE, Cinnamon, Budgie Desktop.

Name:		arc-theme
Version:	20190917
Release:	3%{?dist}
Summary:	A flat theme with transparent elements

License:	GPLv3+
URL:		https://github.com/arc-design/%{name}
Source0:	%{url}/archive/%{version}.tar.gz

BuildArch:	noarch

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	gtk3-devel
BuildRequires:	gtk-murrine-engine
BuildRequires:	inkscape
BuildRequires:	optipng
BuildRequires:	sassc

Requires:	filesystem
Requires:	gnome-themes-extra
Requires:	gtk-murrine-engine

%description
%{common_desc}
	
%prep
%autosetup -p 1
%{_bindir}/autoreconf -fiv

%build	
%configure %{common_configure}
%make_build

%install	
%make_install
	
%files
%license AUTHORS COPYING
%doc README.md
%{_datadir}/themes/*
	
%changelog

* Sat Oct 19 2019 Muhammad Ahmad <muhalantabli@gmail.com>
- All Default DE Support - 20190917-3

* Thu Oct 17 2019 Muhammad Ahmad <muhalantabli@gmail.com>
- Gnome 3.24 Support - 20190917-2

* Sun Oct 13 2019 Muhammad Ahmad <muhalantabli@gmail.com>
- New Release - 20190917-1

