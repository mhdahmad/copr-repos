# copr-repos

<p>Arc Theme</p>

[![Copr build status](https://copr.fedorainfracloud.org/coprs/muhalantabli/copr-repo/package/arc-theme/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/muhalantabli/copr-repo/package/arc-theme/)

<p>Arc Solid Theme</p>

[![Copr build status](https://copr.fedorainfracloud.org/coprs/muhalantabli/copr-repo/package/arc-solid-theme/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/muhalantabli/copr-repo/package/arc-solid-theme/)

<p>Bibata Cursor theme</p>

[![Copr build status](https://copr.fedorainfracloud.org/coprs/muhalantabli/copr-repo/package/bibata-cursor-theme/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/muhalantabli/copr-repo/package/bibata-cursor-theme/)
